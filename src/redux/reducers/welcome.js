import { SHOW_WELCOME, HIDE_WELCOME } from '../actions/WelcomeActions';

const welcomeInitialState = {
  status: true,
  timeAt: new Date().toLocaleString()
};

export const welcomeReducer = (state = welcomeInitialState, action) => {
  switch (action.type) {
    case SHOW_WELCOME:
      return action.payload;

    case HIDE_WELCOME:
      return action.payload;

    default:
      return state;
  }
};
