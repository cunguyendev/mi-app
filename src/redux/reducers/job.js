import { UPDATE_JOB, SEARCH_DATA } from '../actions/JobActions';

const jobInitialState = {
  data: [],
  timeAt: new Date().toLocaleString()
};

export const jobReducer = (state = jobInitialState, action) => {
  switch (action.type) {
    case UPDATE_JOB:
      return action.payload;

    default:
      return state;
  }
};

export const searchReducer = (state = jobInitialState, action) => {
  switch (action.type) {
    case SEARCH_DATA:
      return action.payload;

    default:
      return state;
  }
};
