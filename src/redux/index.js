import { combineReducers } from 'redux';
import { welcomeReducer } from './reducers/welcome';
import { jobReducer, searchReducer } from './reducers/job';

const allReducer = combineReducers({
  welcome: welcomeReducer,
  job: jobReducer,
  searchData: searchReducer
});

export default allReducer;
