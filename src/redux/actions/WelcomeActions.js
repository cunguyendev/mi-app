export const SHOW_WELCOME = 'SHOW_WELCOME';
export const HIDE_WELCOME = 'HIDE_WELCOME';

export const showWelcome = () => ({
  type: SHOW_WELCOME,
  payload: {
    status: true,
    timeAt: new Date().toLocaleString()
  }
});

export const hideWelcome = () => ({
  type: HIDE_WELCOME,
  payload: {
    status: false,
    timeAt: new Date().toLocaleString()
  }
});
