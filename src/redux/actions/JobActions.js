export const UPDATE_JOB = 'UPDATE_JOB';
export const SEARCH_DATA = 'SEARCH_DATA';

export const updateJob = data => dispatch => {
  dispatch({
    type: UPDATE_JOB,
    payload: {
      data: data,
      timeAt: new Date().toLocaleString()
    }
  });
};

export const updateSearchData = (status, data) => dispatch => {
  dispatch({
    type: SEARCH_DATA,
    payload: {
      status: status,
      data: data,
      timeAt: new Date().toLocaleString()
    }
  });
};
