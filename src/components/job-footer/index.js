import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import jobLogo from '../../assets/images/job-logo.png';
import CONSTANTS from '../../utility/constants';
import { goToUrl } from '../../utility/helpers';
import { withRouter } from 'react-router-dom';

const JobFooter = ({ history }) => {
  const goHome = () => {
    goToUrl(history, '/');
  };

  return (
    <Container fluid={true} className="bg-default">
      <Container>
        <Row className="pt-3">
          <Col md={4}>
            <ul className="footer-menu">
              <li onClick={() => goHome()}>
                <p className="hover text-default font-weight-boild">
                  {CONSTANTS.STIE_HOME}
                </p>
              </li>
              <li>
                <a
                  className="hover-color"
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://github.com/cunguyendev"
                >
                  Github
                </a>
              </li>
              <li>
                <a
                  className="hover-color"
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://gitlab.com/cunguyendev"
                >
                  Gitlab
                </a>
              </li>
            </ul>
          </Col>
          <Col md={4} className="text-center">
            <Image src={jobLogo} roundedCircle />
          </Col>
          <Col md={4}>
            <p className="text-default text-right">
              © {new Date().getFullYear()} Mi. All rights reserved.
            </p>
          </Col>
        </Row>
      </Container>
    </Container>
  );
};

export default withRouter(JobFooter);
