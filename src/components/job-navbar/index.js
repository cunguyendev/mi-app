import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import CONSTANTS from '../../utility/constants';
import { withRouter } from 'react-router-dom';
import { goToUrl } from '../../utility/helpers';
import { connect } from 'react-redux';
import { updateSearchData } from '../../redux/actions/JobActions';
import Image from 'react-bootstrap/Image';
import jobLogo from '../../assets/images/job-logo.png';

const JobNavbar = ({ history, updateSearchData }) => {
  const showAll = () => {
    goToUrl(history, '/jobs/positions?page=' + CONSTANTS.DEFAULT_PAGE);
  };

  const goJob = () => {
    updateSearchData(false, []);
    goToUrl(history, '/jobs');
  };

  return (
    <div className="container-fluid bg-default">
      <Navbar className="container" collapseOnSelect expand="lg" variant="dark">
        <Navbar.Brand className="text-default" onClick={() => goJob()}>
          <Image src={jobLogo} roundedCircle /> {CONSTANTS.SITE_JOB}
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto"></Nav>
          <Nav>
            <Nav.Link className="border-right-dotted" onClick={showAll}>
              All jobs
            </Nav.Link>
            <Nav.Link className="border-right-dotted">Post a job</Nav.Link>
            <Nav.Link>How it works</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapActionsToProps = { updateSearchData };

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withRouter(JobNavbar));
