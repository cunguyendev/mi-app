import React, { useState, useRef, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { withRouter } from 'react-router-dom';
import {
  getItemOnURL,
  goToUrl,
  getQueryData,
  getDataBaseOnQuery
} from '../../utility/helpers';
import CONSTANTS from '../../utility/constants';
import { connect } from 'react-redux';
import { updateSearchData } from '../../redux/actions/JobActions';

const JobQuery = ({ history, job, updateSearchData }) => {
  const [typeFilter, setTypeFilter] = useState('full time');
  const descriptionRef = useRef('');
  const locationRef = useRef('');

  useEffect(() => {
    const queryData = getQueryData(document.URL);
    if (getDataBaseOnQuery(queryData, job.data).length) {
      updateSearchData(true, getDataBaseOnQuery(queryData, job.data));
    }
  });

  const handleSearch = () => {
    const currentPage = getItemOnURL(document.URL, 'page');

    if (currentPage) {
      goToUrl(
        history,
        '/jobs/positions?page=' +
          currentPage +
          '&description=' +
          descriptionRef.current.value +
          '&location=' +
          locationRef.current.value +
          '&type=' +
          typeFilter
      );
    } else {
      goToUrl(
        history,
        '/jobs/positions?page=' +
          CONSTANTS.DEFAULT_PAGE +
          '&description=' +
          descriptionRef.current.value +
          '&location=' +
          locationRef.current.value +
          '&type=' +
          typeFilter
      );
    }
  };

  const handleFilter = e => {
    if (e.target.checked) {
      setTypeFilter('contract');
    } else {
      setTypeFilter('full time');
    }
  };

  const { description, location, type } = getQueryData(document.URL);

  return (
    <Container>
      <Form className="job-query">
        <Form.Group>
          <Form.Label>Job Description</Form.Label>
          <Form.Control
            ref={descriptionRef}
            defaultValue={description}
            type="text"
            placeholder="Filter by title, benefits, companies, expertise"
            title="Filter by title, benefits, companies, expertise"
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Location</Form.Label>
          <Form.Control
            ref={locationRef}
            defaultValue={location}
            type="text"
            placeholder="Filter by city, state, zip code or country"
            title="Filter by city, state, zip code or country"
          />
        </Form.Group>

        <Form.Group className="m-2">
          <Form.Check
            onClick={e => handleFilter(e)}
            defaultChecked={type === 'contract' ? true : false}
            type="checkbox"
            label="Contract only"
          />
        </Form.Group>

        <Button onClick={() => handleSearch()} variant="primary" type="button">
          Search
        </Button>
      </Form>
    </Container>
  );
};

const mapStateToProps = state => ({
  job: state.job
});

const mapActionsToProps = { updateSearchData };

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withRouter(JobQuery));
