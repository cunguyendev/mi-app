import React, { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  getContentTitle,
  hotSearchesHandle,
  goToUrl,
  getRandomFromArrayNumber,
  calculatePastTime,
  handlePagination,
  getItemOnURL,
  makeNumber
} from '../../utility/helpers';
import CONSTANTS from '../../utility/constants';

const JobContent = ({ job, history, searchData }) => {
  const [jobListBasic, setJobListBasic] = useState([]);
  const [limitItem, setLimitItem] = useState([]);
  const [page, setPage] = useState(CONSTANTS.DEFAULT_PAGE);
  const { pathname } = history.location;
  const limitCheck =
    makeNumber(getItemOnURL(document.URL, 'page')) * CONSTANTS.LIMITED_ITEM >
    job.data.length;

  useEffect(() => {
    setJobListBasic(
      getRandomFromArrayNumber(job.data, CONSTANTS.LIMITED_JOB_INDEX)
    );

    if (pathname !== '/jobs') {
      if (makeNumber(getItemOnURL(document.URL, 'page'))) {
        setPage(makeNumber(getItemOnURL(document.URL, 'page')));
      } else {
        setPage(CONSTANTS.DEFAULT_PAGE);
        goToUrl(history, '/jobs/positions?page=' + CONSTANTS.DEFAULT_PAGE);
      }
    }
    setLimitItem(handlePagination(job.data, page * CONSTANTS.LIMITED_ITEM));
  }, [job.data, page, history, pathname]);

  const titleContent = getContentTitle(pathname, job.data, page);
  const hotSearches = hotSearchesHandle();

  const showmore = () => {
    let numberPage = page;
    setPage((numberPage += CONSTANTS.NEXT_PAGE));
    goToUrl(history, '/jobs/positions?page=' + numberPage);
  };

  const viewMoreJob = () => {
    setPage(CONSTANTS.DEFAULT_PAGE);
    goToUrl(history, '/jobs/positions?page=' + CONSTANTS.DEFAULT_PAGE);
  };

  const jobBasicItem = jobListBasic.map((item, index) => (
    <JobContentItem
      data={item}
      key={index}
      calculatePastTime={calculatePastTime}
    />
  ));

  const jobItem = limitItem.map((item, index) => (
    <JobContentItem
      data={item}
      key={index}
      calculatePastTime={calculatePastTime}
    />
  ));

  const searchItem = searchData.data.map((item, index) => (
    <JobContentItem
      data={item}
      key={index}
      calculatePastTime={calculatePastTime}
    />
  ));

  const handleGoback = () => {
    goToUrl(history, '/jobs');
  };

  return (
    <Container className="job-content">
      <Row>
        <Col md={12}>
          <JobContentTitle
            title={
              searchData.status
                ? 'Your result...'
                : !limitCheck
                ? titleContent
                : 'Oh man! The page not found!'
            }
          />
        </Col>
        <Col md={12}>
          <Row className="mt-2">
            <Col md={8}>
              {!limitCheck ? (
                searchData.status ? (
                  searchItem
                ) : pathname === '/jobs' ? (
                  jobBasicItem
                ) : (
                  jobItem
                )
              ) : (
                <p>
                  Something went wrong with your navigation!{' '}
                  <b className="go-back" onClick={() => handleGoback()}>
                    Go home!
                  </b>
                </p>
              )}
              {!searchData.status &&
                pathname === '/jobs/positions' &&
                (page * 10 >= job.data.length ? null : (
                  <Button
                    onClick={() => showmore()}
                    className="mt-2 w-100"
                    variant="primary"
                    type="button"
                  >
                    More Awesome Jobs
                  </Button>
                ))}
              {pathname !== '/jobs/positions' && (
                <React.Fragment>
                  <Row className="job-content-item ml-0">
                    <Col className="pl-0" md={12}>
                      <p
                        className="job-content-item-more"
                        onClick={() => viewMoreJob()}
                      >
                        More Awesome Jobs →
                      </p>
                    </Col>
                  </Row>
                  <Row className="job-content-item ml-0">
                    <Col className="pl-0" md={12}>
                      <JobContentTitle title="Hot Searches" />
                    </Col>
                    <Col className="pl-0 font-weight-boild" md={12}>
                      {hotSearches}
                    </Col>
                  </Row>
                </React.Fragment>
              )}
            </Col>
            <Col md={4} className="job-content-subscribe">
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://github.com/cunguyendev"
                className="job-content-subscribe-item"
              >
                <i className="fa fa-github"></i> View more on my Github
              </a>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://gitlab.com/cunguyendev"
                className="job-content-subscribe-item"
              >
                <i className="fa fa-gitlab"></i> View more on my Gitlab
              </a>
              <div className="job-content-subscribe-email">
                <p className="job-content-subscribe-email-title">
                  Subscribe to email updates
                </p>
                <p className="job-content-subscribe-email-desc">
                  Subscribe and we’ll send you a summary once a week if new jobs
                  are posted to this list.
                </p>
                <Button
                  className="job-content-subscribe-email-submit"
                  variant="primary"
                  type="button"
                >
                  Subscribe to updates
                </Button>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export const JobContentTitle = ({ title }) => (
  <p className="job-content-title">{title}</p>
);

export const JobContentItem = ({ data, calculatePastTime }) => {
  const { title, type, company, location, created_at } = data;

  return (
    <Row className="job-content-item ml-0">
      <Col className="pl-0" md={8}>
        <p className="job-content-item-position">{title}</p>
        <p className="job-content-item-company">
          {company} – <b className="text-default">{type}</b>
        </p>
      </Col>
      <Col className="pr-0" md={4}>
        <p className="job-content-item-location">{location}</p>
        <p className="job-content-item-timeline">
          {calculatePastTime(created_at)}
        </p>
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  job: state.job,
  searchData: state.searchData
});

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withRouter(JobContent));
