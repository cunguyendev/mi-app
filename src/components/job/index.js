import React, { useEffect } from 'react';
import JobNavbar from '../job-navbar';
import JobQuery from '../job-query';
import JobContent from '../job-content';
import joblist from '../../assets/dataset/positions.json';
import { connect } from 'react-redux';
import { updateJob } from '../../redux/actions/JobActions';
import CONSTANTS from '../../utility/constants';
import JobFooter from '../job-footer';

const Job = ({ updateJob, job }) => {
  useEffect(() => {
    document.title = CONSTANTS.SITE_JOB;

    updateJob(joblist);
  }, [updateJob]);

  return (
    <React.Fragment>
      <JobNavbar />
      <JobQuery />
      <JobContent />
      <JobFooter />
    </React.Fragment>
  );
};

const mapStateToProps = state => ({
  welcome: state.welcome
});

const mapActionsToProps = { updateJob };

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Job);
