import React from 'react';
import './index.scss';

const OverlayAnimation = () => {
  let elements = [];

  for (let i = 0; i <= 10; i++) {
    elements.push(<li key={i}></li>);
  }

  return (
    <div className="overlay">
      <div className="area">
        <ul className="circles">{elements}</ul>
      </div>
    </div>
  );
};

export default OverlayAnimation;
