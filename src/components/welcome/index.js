import React, { useEffect, useState } from 'react';
import './index.scss';
import CONSTANTS from '../../utility/constants';
import { hideWelcome } from '../../redux/actions/WelcomeActions';
import { connect } from 'react-redux';

const Welcome = ({ hideWelcome, title }) => {
  const [timeOut, setTimeOut] = useState(0);

  useEffect(() => {
    const timeDown = setInterval(() => tick(), CONSTANTS.COUNT_DOWN);

    const tick = () => {
      if (timeOut <= 5) {
        setTimeOut(timeOut + 1);
      } else {
        hideWelcome();
        clearInterval(timeDown);
      }
    };

    return () => {
      clearInterval(timeDown);
    };
  });

  return (
    <div className="welcome">
      <span className="welcome-title">Welcome to</span>
      <span className="welcome-title-sub">{title}</span>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapActionsToProps = {
  hideWelcome
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Welcome);
