import React, { useEffect } from 'react';
import Welcome from './components/welcome';
import './App.scss';
import CONSTANTS from './utility/constants';
import OverlayAnimation from './components/overlay-animation';
import { connect } from 'react-redux';
import { showWelcome } from './redux/actions/WelcomeActions';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Job from './components/job';

const App = ({ showWelcome, welcome }) => {
  useEffect(() => {
    showWelcome();
  }, [showWelcome]);

  return (
    <React.Fragment>
      <Router>
        <Route exact path="/" component={Home} />
        <Route
          path="/jobs"
          render={props => <Jobs {...props} welcome={welcome} />}
        />
        <Route path="/topics" component={Home} />
      </Router>
    </React.Fragment>
  );
};

const Home = () => {
  document.title = CONSTANTS.STIE_HOME;

  return (
    <React.Fragment>
      <OverlayAnimation />
      <Welcome title={CONSTANTS.STIE_HOME} />
    </React.Fragment>
  );
};

const Jobs = ({ welcome }) => (
  // welcome.status ? (
  //   <React.Fragment>
  //     <OverlayAnimation />
  //     <Welcome title={CONSTANTS.SITE_JOB} />
  //   </React.Fragment>
  // ) : (
  //   <Job />
  // );

  <Job />
);

const mapStateToProps = state => ({
  welcome: state.welcome
});

const mapActionsToProps = {
  showWelcome
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(App);
