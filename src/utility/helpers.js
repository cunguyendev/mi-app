import React from 'react';
import CONSTANTS from './constants';
import moment from 'moment';

export const getContentTitle = (pathName, data, others) => {
  switch (pathName) {
    case '/jobs':
      return 'Featured Jobs';

    case '/jobs/positions':
      return (
        'Showing ' +
        CONSTANTS.NEXT_PAGE +
        ' - ' +
        others * CONSTANTS.LIMITED_ITEM +
        ' of ' +
        data.length +
        ' jobs'
      );

    default:
      return '';
  }
};

export const hotSearchesHandle = () => {
  const data = [];
  CONSTANTS.JOB_MAJOR.map((item, index) => {
    data.push(
      <a className="job-searches-item" key={index} href="#g">
        {item}
      </a>
    );

    if (index >= 0 && index < CONSTANTS.JOB_MAJOR.length - 1) {
      data.push(<React.Fragment key={item}> · </React.Fragment>);
    }

    return true;
  });

  return data;
};

export const goToUrl = (goTo, link) => {
  goTo.push(link);
};

export const getItemOnURL = (url, item) => new URL(url).searchParams.get(item);

export const getRandomFromArrayNumber = (arr, size) => {
  let shuffled = arr.slice(0),
    i = arr.length,
    temp,
    index;
  while (i--) {
    index = Math.floor((i + 1) * Math.random());
    temp = shuffled[index];
    shuffled[index] = shuffled[i];
    shuffled[i] = temp;
  }
  return shuffled.slice(0, size);
};

export const calculatePastTime = time => moment(new Date(time)).fromNow();

export const handlePagination = (data, limit) => {
  let count = 0;
  let dataList = [];

  data.map(item => {
    if (count < limit) {
      dataList.push(item);
      count++;
    }

    return true;
  });

  return dataList;
};

export const makeNumber = data => parseInt(data, CONSTANTS.DECIMAL);

export const getQueryData = url => {
  return {
    page: getItemOnURL(url, 'page'),
    description: getItemOnURL(url, 'description'),
    location: getItemOnURL(url, 'location'),
    type: getItemOnURL(url, 'type')
  };
};

export const getDataBaseOnQuery = (queryData, data) => {
  const { description, location, type } = queryData;
  let dataSearch = [];

  if (description) {
    data.map((item, index) => {
      if (
        item.title
          .toLowerCase()
          .indexOf(queryData.description.toLowerCase()) !== -1 ||
        item.company
          .toLowerCase()
          .indexOf(queryData.description.toLowerCase()) !== -1 ||
        item.description
          .toLowerCase()
          .indexOf(queryData.description.toLowerCase()) !== -1
      ) {
        dataSearch[index] = item;
      }

      return true;
    });
  }

  if (location) {
    data.map((item, index) => {
      if (
        item.location
          .toLowerCase()
          .indexOf(queryData.location.toLowerCase()) !== -1
      ) {
        dataSearch[index] = item;
      }

      return true;
    });
  }

  if (type) {
    data.map((item, index) => {
      if (
        item.type.toLowerCase().indexOf(queryData.type.toLowerCase()) !== -1
      ) {
        dataSearch[index] = item;
      }

      return true;
    });
  }

  return dataSearch;
};
