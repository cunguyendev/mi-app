const CONSTANTS = {
  SITE_NAME: 'Mi App',
  STIE_HOME: 'Mi Home',
  SITE_JOB: 'Mi Jobs',
  COUNT_DOWN: 1000,
  DECIMAL: 10,
  LIMITED_ITEM: 10,
  LIMITED_JOB_INDEX: 2,
  NEXT_PAGE: 1,
  DEFAULT_PAGE: 1,
  JOB_MAJOR: [
    'PHP',
    'Rails',
    'Python',
    'JavaScript',
    'ScalaAndroid',
    'iOS',
    'Linux',
    'Erlang San Francisco',
    'New York City',
    'Austin',
    'TX',
    'London',
    'Europe'
  ]
};

export default CONSTANTS;
